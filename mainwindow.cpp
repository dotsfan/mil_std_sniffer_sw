#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    MainWindow::setWindowTitle("Монитор МКПД");
    MainWindow::setWindowFlags(Qt::WindowStaysOnTopHint);

    this->UdpClient = std::make_unique<Client>();
    UdpClient->setSocket(mcu_ip, mcu_port);

    time = QTime::currentTime();
    time.start();

    date = QDate::currentDate();

    initPushBtns();
    initTables();
    initLables();

    saveTreeWidgetItems();
    clearTreeWidgetStat();

    ui->plainTextEdit_terminal->clear();

    connect(this, SIGNAL(clearMsgStat()), UdpClient.get(), SLOT(clearMsgStat()));

    connect(UdpClient.get(), SIGNAL(terminalWrite(QString)), this, SLOT(terminalWrite(QString)));
    connect(UdpClient.get(), SIGNAL(setLabelMonitorStatus(Client::MonitorStatus)), this, SLOT(setLabelMonitorStatus(Client::MonitorStatus)));
    connect(UdpClient.get(), SIGNAL(setTableMonitorItem(QStringList)), this, SLOT(setTableMonitorItem(QStringList)));
    connect(UdpClient.get(), SIGNAL(setMsgStat(Client::MsgStat)), this, SLOT(setMsgStat(Client::MsgStat)));

    // Установка директории для записи статистики по ответам

    dir.mkpath("LOG/" + date.toString("yyyy-MM-dd"));
    dir.setPath("LOG/" + date.toString("yyyy-MM-dd"));

    emit clearMsgStat();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/********************* START PRIVATE FUNCTIONS DEFINITIONS ********************/
void MainWindow::initPushBtns()
{
    ui->pushButton_runStopMonitor->setCheckable(true);
    ui->pushButton_runStopMonitor->setChecked(false);
    ui->pushButton_runStopMonitor->setText("Запустить монитор");
}

void MainWindow::initTables()
{
    ui->tableWidget_monitor->horizontalHeader()->resizeSection(ITEM_OP_MODE_IND, 100);
    ui->tableWidget_monitor->horizontalHeader()->resizeSection(ITEM_CMD_CODE_IND, 150);
    ui->tableWidget_monitor->horizontalHeader()->resizeSection(ITEM_DATA_IND, 350);
    ui->tableWidget_monitor->horizontalHeader()->resizeSection(ITEM_MSG_STATUS_IND, 75);
}

void MainWindow::initLables()
{
    setLabelMonitorStatus(Client::monitor_fail);
}

void MainWindow::saveTreeWidgetItems()
{
    ui->treeWidget_stat->expandAll();    

    QTreeWidgetItemIterator it(ui->treeWidget_stat);
    items_stat_list.clear();

    while(*it)
    {
        items_stat_list.append(*it++);
    }
}

void MainWindow::clearTreeWidgetStat()
{
    ui->treeWidget_stat->expandAll();

    items_stat_list.at(TOTAL_MSG_IND)->setText(0, total_msg);
    items_stat_list.at(FORMAT_MSG_IND)->setText(0, format_msg);

    items_stat_list.at(INCORRECT_MSG_IND)->setText(0, incorrect_msg);
    items_stat_list.at(CORRECT_MSG_IND)->setText(0, correct_msg);

    items_stat_list.at(BC_TO_RT_IND)->setText(0, bc_to_rt_msg);
    items_stat_list.at(RT_TO_BC_IND)->setText(0, rt_to_bc_msg);
    items_stat_list.at(RT_TO_RT_IND)->setText(0, rt_to_rt_msg);
    items_stat_list.at(CMD_CNTRL_IND)->setText(0, cmd_cntrl_msg);

    items_stat_list.at(FORMAT_01_IND)->setText(0, format_01_msg);
    items_stat_list.at(FORMAT_02_IND)->setText(0, format_02_msg);
    items_stat_list.at(FORMAT_03_IND)->setText(0, format_03_msg);
    items_stat_list.at(FORMAT_04_IND)->setText(0, format_04_msg);
    items_stat_list.at(FORMAT_05_IND)->setText(0, format_05_msg);
    items_stat_list.at(FORMAT_06_IND)->setText(0, format_06_msg);
    items_stat_list.at(FORMAT_07_IND)->setText(0, format_07_msg);
    items_stat_list.at(FORMAT_08_IND)->setText(0, format_08_msg);
    items_stat_list.at(FORMAT_09_IND)->setText(0, format_09_msg);
    items_stat_list.at(FORMAT_10_IND)->setText(0, format_10_msg);
}
/********************** END PRIVATE FUNCTIONS DEFINITIONS *********************/

/**************** START PRIVATE SLOTS FUNCTIONS DEFINITIONS *******************/
void MainWindow::terminalWrite(const QString &data)
{
    if(data.isEmpty())
        return;
    ui->plainTextEdit_terminal->moveCursor(QTextCursor::Start);
    ui->plainTextEdit_terminal->insertPlainText(data);
}

void MainWindow::setLabelMonitorStatus(const Client::MonitorStatus &monitor_status)
{
    switch (monitor_status)
    {
        case Client::monitor_ok:
            ui->label_monitor->setText(monitor_ok);
            ui->label_monitor->setStyleSheet(label_monitor_ok);
            break;

        case Client::monitor_fail:
            ui->label_monitor->setText(monitor_fail);
            ui->label_monitor->setStyleSheet(label_monitor_fail);
            break;

        default: break;
    }
}

void MainWindow::setTableMonitorItem(const QStringList &data)
{    
    QTableWidgetItem *item_time = new QTableWidgetItem(data.at(ITEM_TIME_IND));
    QTableWidgetItem *item_format = new QTableWidgetItem(data.at(ITEM_FORMAT_IND));
    QTableWidgetItem *item_addr_rt = new QTableWidgetItem(data.at(ITEM_ADDR_RT_IND));
    QTableWidgetItem *item_op_mode = new QTableWidgetItem(data.at(ITEM_OP_MODE_IND));
    QTableWidgetItem *item_cmd_code = new QTableWidgetItem(data.at(ITEM_CMD_CODE_IND));
    QTableWidgetItem *item_data = new QTableWidgetItem(data.at(ITEM_DATA_IND));
    QTableWidgetItem *item_msg_status = new QTableWidgetItem(data.at(ITEM_MSG_STATUS_IND));

    item_time->setTextAlignment(Qt::AlignCenter);
    item_format->setTextAlignment(Qt::AlignCenter);
    item_addr_rt->setTextAlignment(Qt::AlignCenter);
    item_op_mode->setTextAlignment(Qt::AlignCenter);
    item_cmd_code->setTextAlignment(Qt::AlignCenter);
    item_msg_status->setTextAlignment(Qt::AlignCenter);

    ui->tableWidget_monitor->insertRow(row_count);
    ui->tableWidget_monitor->setItem(row_count, ITEM_TIME_IND, item_time);
    ui->tableWidget_monitor->setItem(row_count, ITEM_FORMAT_IND, item_format);
    ui->tableWidget_monitor->setItem(row_count, ITEM_ADDR_RT_IND, item_addr_rt);
    ui->tableWidget_monitor->setItem(row_count, ITEM_OP_MODE_IND, item_op_mode);
    ui->tableWidget_monitor->setItem(row_count, ITEM_CMD_CODE_IND, item_cmd_code);
    ui->tableWidget_monitor->setItem(row_count, ITEM_DATA_IND, item_data);
    ui->tableWidget_monitor->setItem(row_count++, ITEM_MSG_STATUS_IND, item_msg_status);

    ui->tableWidget_monitor->scrollToBottom();
}

void MainWindow::setMsgStat(const Client::MsgStat &msg_stat)
{
    items_stat_list.at(TOTAL_MSG_IND)->setText(0, total_msg + QString::number(msg_stat.correct + msg_stat.incorrect));

    items_stat_list.at(INCORRECT_MSG_IND)->setText(0, incorrect_msg + QString::number(msg_stat.incorrect));
    items_stat_list.at(CORRECT_MSG_IND)->setText(0, correct_msg + QString::number(msg_stat.correct));

    items_stat_list.at(BC_TO_RT_IND)->setText(0, bc_to_rt_msg + QString::number(msg_stat.bc2rt));
    items_stat_list.at(RT_TO_BC_IND)->setText(0, rt_to_bc_msg + QString::number(msg_stat.rt2bc));
    items_stat_list.at(RT_TO_RT_IND)->setText(0, rt_to_rt_msg + QString::number(msg_stat.rt2rt));
    items_stat_list.at(CMD_CNTRL_IND)->setText(0, cmd_cntrl_msg + QString::number(msg_stat.cmdCntrl));

    items_stat_list.at(FORMAT_01_IND)->setText(0, format_01_msg + QString::number(msg_stat.format01));
    items_stat_list.at(FORMAT_02_IND)->setText(0, format_02_msg + QString::number(msg_stat.format02));
    items_stat_list.at(FORMAT_03_IND)->setText(0, format_03_msg + QString::number(msg_stat.format03));
    items_stat_list.at(FORMAT_04_IND)->setText(0, format_04_msg + QString::number(msg_stat.format04));
    items_stat_list.at(FORMAT_05_IND)->setText(0, format_05_msg + QString::number(msg_stat.format05));
    items_stat_list.at(FORMAT_06_IND)->setText(0, format_06_msg + QString::number(msg_stat.format06));
    items_stat_list.at(FORMAT_07_IND)->setText(0, format_07_msg + QString::number(msg_stat.format07));
    items_stat_list.at(FORMAT_08_IND)->setText(0, format_08_msg + QString::number(msg_stat.format08));
    items_stat_list.at(FORMAT_09_IND)->setText(0, format_09_msg + QString::number(msg_stat.format09));
    items_stat_list.at(FORMAT_10_IND)->setText(0, format_10_msg + QString::number(msg_stat.format10));
}

void MainWindow::on_pushButton_clearTerm_clicked()
{
    ui->plainTextEdit_terminal->clear();
}

void MainWindow::on_pushButton_runStopMonitor_clicked()
{
    if(ui->pushButton_runStopMonitor->isChecked())
    {
        ui->pushButton_runStopMonitor->setText("Остановить монитор");
        if(UdpClient->sendUdpData(UdpClient->getMonitorRunData(), UdpClient->getMonitorRunDataSize()))
        {
            time.start();
            terminalWrite(time.toString("HH:mm:ss:zzz") + ": " + "[UDP_ERR]: Ошибка запуска монитора");
            return;
        }
        // Установим статус по приему корректного ответа
    }
    else
    {
         ui->pushButton_runStopMonitor->setText("Запустить монитор");
         if(UdpClient->sendUdpData(UdpClient->getMonitorStopData(), UdpClient->getMonitorStopDataSize()))
         {
             time.start();
             terminalWrite(time.toString("HH:mm:ss:zzz") + ": " + "[UDP_ERR]: Ошибка остановки монитора");
             return;
         }
         setLabelMonitorStatus(Client::monitor_fail);
    }
}

void MainWindow::on_pushButton_clearData_clicked()
{
    ui->tableWidget_monitor->clearContents();
    ui->tableWidget_monitor->model()->removeRows(0, ui->tableWidget_monitor->rowCount());
    row_count = 0;
}

void MainWindow::on_pushButton_resetStat_clicked()
{
    clearTreeWidgetStat();
    emit clearMsgStat();
}

void MainWindow::on_pushButton_saveData_clicked()
{
    if(!ui->tableWidget_monitor->rowCount()) // Данных нет, ничего не сохраняем
        return;

    QFile file;
    time.start();
    file.setFileName(dir.filePath(date.toString("yyyy-MM-dd") + "_" + time.toString("HH-mm-ss") + ".txt"));
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    // Сохраняем шапку
    if(file.write(title_data_file) == -1)
        return;

    QString data("");
    QTableWidgetItem *item = new QTableWidgetItem();
    for(quint8 row=0; row<ui->tableWidget_monitor->rowCount(); row++)
    {
        data = "";
        for(quint8 column=0; column<ui->tableWidget_monitor->columnCount(); column++)
        {
            item = ui->tableWidget_monitor->item(row, column);
            if(column == 2)
                data += "\t";
            else if((column == 3) || (column == 4))
                data += "\t\t";
            else if(column == 5)
                data += "\t\t\t\t\t";
            data += "\t" + item->text();
        }
        data += "\n";
        file.write(data.toUtf8());
    }
    file.close();

    QMessageBox msg_box;
    msg_box.setText("Данные сохранены!");
    msg_box.setDefaultButton(QMessageBox::Ok);
    msg_box.setWindowFlags(Qt::WindowStaysOnTopHint);

    msg_box.exec();
}

void MainWindow::on_pushButton_saveTerm_clicked()
{
    if(ui->plainTextEdit_terminal->toPlainText().isEmpty())
        return;

    QFile file;
    time.start();
    file.setFileName(dir.filePath(date.toString("yyyy-MM-dd") + "_" + time.toString("HH-mm-ss") + ".log"));
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    file.write(ui->plainTextEdit_terminal->toPlainText().toUtf8());
    file.close();

    QMessageBox msg_box;
    msg_box.setText("Данные сохранены!");
    msg_box.setDefaultButton(QMessageBox::Ok);
    msg_box.setWindowFlags(Qt::WindowStaysOnTopHint);

    msg_box.exec();
}
/****************** END PRIVATE SLOTS FUNCTIONS DEFINITIONS *******************/
