#include "client.h"

Client::Client(QObject *parent) : QObject(parent)
{
    this->client = std::make_unique<QUdpSocket>();

    time = QTime::currentTime();
    time.start();

    connect(client.get(), SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
}

/******************** START PUBLIC FUNCTIONS DEFINITIONS **********************/
void Client::setSocket(const QString ip, const quint16 port)
{
    server_ip.setAddress(ip);
    server_port = port;
}

QString Client::charDatatToHex(const uchar *char_data, const size_t data_size)
{
    QString str("");
    QString cur_symb("");
    for(size_t i=0; i<data_size; i++)
    {
        if(char_data[i] == 0)
        {
            str += "00 ";
            continue;
        }
        cur_symb = QString::number(char_data[i], 16);
        if(cur_symb.length() == 1)
        {
            cur_symb.prepend('0');
        }
        str += cur_symb + " ";
    }
    return str.trimmed().toUpper();
}

quint8 Client::calculateCRC(const char *data, const quint64 data_size)
{
    quint8 crc = 0;
    for(quint8 i=0; i<data_size; i++)
        crc ^= *data++;

    return crc;
}

quint8 Client::sendUdpData(const uchar *udp_data, const quint8 data_size)
{
    if(client->writeDatagram((char *)udp_data, data_size, server_ip, server_port) == -1)
        return 1;

    return 0;
}

const uchar *Client::getMonitorRunData()
{
    return data_monitor_run;
}

quint8 Client::getMonitorRunDataSize()
{
    return DATA_RUN_MONITOR_SIZE;
}

const uchar *Client::getMonitorStopData()
{
    return data_monitor_stop;
}

quint8 Client::getMonitorStopDataSize()
{
    return DATA_STOP_MONITOR_SIZE;
}
/********************* END PUBLIC FUNCTIONS DEFINITIONS ***********************/

/******************** START PRIVATE FUNCTIONS DEFINITIONS *********************/
bool Client::checkFormatMsg(const quint8 format, const quint8 flags)
{
    bool status = false;
    quint8 START_RX = MilStdFlags::RX_OK | MilStdFlags::CMD1_OK;

    switch (format) {
    case 1:
        msg_stat_s.format01++;
        msg_stat_s.bc2rt++;
        if(flags == (START_RX | MilStdFlags::DATA_OK | MilStdFlags::REPLY1_OK))
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    case 2:
        msg_stat_s.format02++;
        msg_stat_s.rt2bc++;
        if(flags == (START_RX | MilStdFlags::REPLY1_OK | MilStdFlags::DATA_OK))
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    case 3:
        msg_stat_s.format03++;
        msg_stat_s.rt2rt++;
        if(flags == (START_RX | MilStdFlags::CMD2_OK | MilStdFlags::REPLY1_OK | MilStdFlags::DATA_OK | MilStdFlags::REPLY2_OK))
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    case 4:
        msg_stat_s.format04++;
        msg_stat_s.cmdCntrl++;
        if(flags == (START_RX | MilStdFlags::REPLY1_OK))
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    case 5:
        msg_stat_s.format05++;
        msg_stat_s.cmdCntrl++;
        if(flags == (START_RX | MilStdFlags::REPLY1_OK | MilStdFlags::CNTRL_DATA_OK))
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    case 6:
        msg_stat_s.format06++;
        msg_stat_s.cmdCntrl++;
        if(flags == (START_RX | MilStdFlags::CNTRL_DATA_OK | MilStdFlags::REPLY1_OK))
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    case 7:
        msg_stat_s.format07++;
        msg_stat_s.bc2rt++;
        if(flags == (START_RX | MilStdFlags::DATA_OK))
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    case 8:
        msg_stat_s.format08++;
        msg_stat_s.rt2rt++;
        if(flags == (START_RX | MilStdFlags::CMD2_OK | MilStdFlags::REPLY1_OK | MilStdFlags::DATA_OK))
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    case 9:
        msg_stat_s.format09++;
        msg_stat_s.cmdCntrl++;
        if(flags == START_RX)
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    case 10:
        msg_stat_s.format10++;
        msg_stat_s.cmdCntrl++;
        if(flags == (START_RX | MilStdFlags::CNTRL_DATA_OK))
        {
            msg_stat_s.correct++;
            status = true;
        }
        else
            msg_stat_s.incorrect++;
        break;

    default: break;
    }

    return status;
}

QString Client::checkBusErrors(const QString time, const quint8 format, const quint8 bus_err)
{
    QString data("");

    if((format == 0xee) || (bus_err))
    {
        data = time + "[BUS_ERR]: Ошибка транзакции на шине\n";

        if(bus_err & BusErrorsReg::NORCV)
            data += time + bus_errors_msg[NORCV_POS];

        if(bus_err & BusErrorsReg::MAN_ERR)
            data += time + bus_errors_msg[MANER_POS];

        if(bus_err & BusErrorsReg::SYNCH_ERR)
            data += time + bus_errors_msg[SYNCH_ERR_POS];

        if(bus_err & BusErrorsReg::SEQ_ERR)
            data += time + bus_errors_msg[SEQ_ERR_POS];

        if(bus_err & BusErrorsReg::GAP_ERR)
            data += time + bus_errors_msg[GAP_ERR_POS];

        if(bus_err & BusErrorsReg::CON_ERR)
            data += time + bus_errors_msg[CON_ERR_POS];

        if(bus_err & BusErrorsReg::PRO_ERR)
            data += time + bus_errors_msg[PRO_ERR_POS];
    }

    return data;
}

QString Client::processFormatMsg(const QString time, const quint8 format, const quint8 flags)
{
    QString data("");
    switch(format) // Обработка формата сообщения
    {
        case 1: // Для формата 1 ожидается прием СД и ОС1
            if(!(flags & MilStdFlags::DATA_OK))
                data += time + mil_std_msg[DATA_OK_POS];

            if(!(flags & MilStdFlags::REPLY1_OK))
                data += time + mil_std_msg[REPLY1_OK_POS];

            // Проверка ложных КС2, ОС2, СД КУ
            if(flags & MilStdFlags::CMD2_OK)
                data += time + mil_std_err[CMD2_ERR_POS];

            if(flags & MilStdFlags::REPLY2_OK)
                data += time + mil_std_err[REPLY2_ERR_POS];

            if(flags & MilStdFlags::CNTRL_DATA_OK)
                data += time + mil_std_err[CNTRL_DATA_ERR_POS];
            break;

        case 2: // Для формата 2 ожидается прием ОС1 и СД
            if(!(flags & MilStdFlags::REPLY1_OK))
                data += time + mil_std_msg[REPLY1_OK_POS];

            if(!(flags & MilStdFlags::DATA_OK))
                data += time + mil_std_msg[DATA_OK_POS];

            // Проверка ложных КС2, ОС2, СД КУ
            if(flags & MilStdFlags::CMD2_OK)
                data += time + mil_std_err[CMD2_ERR_POS];

            if(flags & MilStdFlags::REPLY2_OK)
                data += time + mil_std_err[REPLY2_ERR_POS];

            if(flags & MilStdFlags::CNTRL_DATA_OK)
                data += time + mil_std_err[CNTRL_DATA_ERR_POS];
            break;

        case 3: // Для формата 3 ожидается прием КС2, ОС1, СД, ОС2
            if(!(flags & MilStdFlags::CMD2_OK))
                data += time + mil_std_msg[CMD2_OK_POS];

            if(!(flags & MilStdFlags::REPLY1_OK))
                data += time + mil_std_msg[REPLY1_OK_POS];

            if(!(flags & MilStdFlags::DATA_OK))
                data += time + mil_std_msg[DATA_OK_POS];

            if(!(flags & MilStdFlags::REPLY2_OK))
                data += time + mil_std_msg[REPLY2_OK_POS];

            // Проверка ложных СД КУ
            if(flags & MilStdFlags::CNTRL_DATA_OK)
                data += time + mil_std_err[CNTRL_DATA_ERR_POS];
            break;

        case 4: // Для формата 4 ожидается прием ОС1
            if(!(flags & MilStdFlags::REPLY1_OK))
                data += time + mil_std_msg[REPLY1_OK_POS];

            // Проверка ложных КС2, ОС2, СД, СД КУ
            if(flags & MilStdFlags::CMD2_OK)
                data += time + mil_std_err[CMD2_ERR_POS];

            if(flags & MilStdFlags::REPLY2_OK)
                data += time + mil_std_err[REPLY2_ERR_POS];

            if(flags & MilStdFlags::DATA_OK)
                data += time + mil_std_err[DATA_ERR_POS];

            if(flags & MilStdFlags::CNTRL_DATA_OK)
                data += time + mil_std_err[CNTRL_DATA_ERR_POS];
            break;

        case 5: // Для формата 5 ожидается прием ОС1 и СД КУ
            if(!(flags & MilStdFlags::REPLY1_OK))
                data += time + mil_std_msg[REPLY1_OK_POS];

            if(!(flags & MilStdFlags::CNTRL_DATA_OK))
                data += time + mil_std_msg[CNTRL_DATA_OK_POS];

            // Проверка ложных КС2, ОС2, СД
            if(flags & MilStdFlags::CMD2_OK)
                data += time + mil_std_err[CMD2_ERR_POS];

            if(flags & MilStdFlags::REPLY2_OK)
                data += time + mil_std_err[REPLY2_ERR_POS];

            if(flags & MilStdFlags::DATA_OK)
                data += time + mil_std_err[DATA_ERR_POS];
            break;

        case 6: // Для формата 6 ожидается прием СД КУ и ОС1
            if(!(flags & MilStdFlags::CNTRL_DATA_OK))
                data += time + mil_std_msg[CNTRL_DATA_OK_POS];

            if(!(flags & MilStdFlags::REPLY1_OK))
                data += time + mil_std_msg[REPLY1_OK_POS];

            // Проверка ложных КС2, ОС2, СД
            if(flags & MilStdFlags::CMD2_OK)
                data += time + mil_std_err[CMD2_ERR_POS];

            if(flags & MilStdFlags::REPLY2_OK)
                data += time + mil_std_err[REPLY2_ERR_POS];

            if(flags & MilStdFlags::DATA_OK)
                data += time + mil_std_err[DATA_ERR_POS];
            break;

        case 7: // Для формата 7 ожидается прием СД
            if(!(flags & MilStdFlags::DATA_OK))
                data += time + mil_std_msg[DATA_OK_POS];

            // Проверка ложных КС2, ОС1, ОС2, СД КУ
            if(flags & MilStdFlags::CMD2_OK)
                data += time + mil_std_err[CMD2_ERR_POS];

            if(flags & MilStdFlags::REPLY1_OK)
                data += time + mil_std_err[REPLY1_ERR_POS];

            if(flags & MilStdFlags::REPLY2_OK)
                data += time + mil_std_err[REPLY2_ERR_POS];

            if(flags & MilStdFlags::CNTRL_DATA_OK)
                data += time + mil_std_err[CNTRL_DATA_ERR_POS];
            break;

        case 8: // Для формата 8 ожидается прием КС2, ОС1, СД
            if(!(flags & MilStdFlags::CMD2_OK))
                data += time+ mil_std_msg[CMD2_OK_POS];

            if(!(flags & MilStdFlags::REPLY1_OK))
                data += time + mil_std_msg[REPLY1_OK_POS];

            if(!(flags & MilStdFlags::DATA_OK))
                data += time + mil_std_msg[DATA_OK_POS];

            // Проверка ложных ОС2, СД КУ
            if(flags & MilStdFlags::REPLY2_OK)
                data += time + mil_std_err[REPLY2_ERR_POS];

            if(flags & MilStdFlags::CNTRL_DATA_OK)
                data += time + mil_std_err[CNTRL_DATA_ERR_POS];
            break;

        case 9: // Для формата 9 не ожидаются никаких данных, кроме принятого КС1
            // Проверка ложных КС2, ОС1, ОС2, СД, СД КУ
            if(flags & MilStdFlags::CMD2_OK)
                data += time + mil_std_err[CMD2_ERR_POS];

            if(flags & MilStdFlags::REPLY1_OK)
                data += time + mil_std_err[REPLY1_ERR_POS];

            if(flags & MilStdFlags::REPLY2_OK)
                data += time + mil_std_err[REPLY2_ERR_POS];

            if(flags & MilStdFlags::DATA_OK)
                data += time + mil_std_err[DATA_ERR_POS];

            if(flags & MilStdFlags::CNTRL_DATA_OK)
                data += time + mil_std_err[CNTRL_DATA_ERR_POS];
            break;

        case 10: // Для формата 10 ожидается прием СД КУ
            if(!(flags & MilStdFlags::CNTRL_DATA_OK))
                data += time + mil_std_msg[CNTRL_DATA_OK_POS];

            // Проверка ложных КС2, ОС1, ОС2, СД
            if(flags & MilStdFlags::CMD2_OK)
                data += time + mil_std_err[CMD2_ERR_POS];

            if(flags & MilStdFlags::REPLY1_OK)
                data += time + mil_std_err[REPLY1_ERR_POS];

            if(flags & MilStdFlags::REPLY2_OK)
                data += time + mil_std_err[REPLY2_ERR_POS];

            if(flags & MilStdFlags::DATA_OK)
                data += time + mil_std_err[DATA_ERR_POS];
            break;

        default: break;
    }

    return data;
}
/********************* END PRIVATE FUNCTIONS DEFINITIONS **********************/

/***************** START PRIVATE SLOTS FUNCTIONS DEFINITIONS ******************/

void Client::readPendingDatagrams()
{
    while(client->hasPendingDatagrams())
    {
        QHostAddress sender_ip;
        quint16 sender_port;
        QByteArray udp_datagram_rx;

        quint64 udp_size_rx = client->pendingDatagramSize();
        udp_datagram_rx.resize(udp_size_rx);

        time.start();
        QString time_str = time.toString("HH:mm:ss:zzz") + " ";

        if(client->readDatagram(udp_datagram_rx.data(), udp_datagram_rx.size(), &sender_ip, &sender_port) == -1)
        {
            emit terminalWrite(time_str + "[UDP_ERR]: Ошибка чтения\n");
            continue;
        }
        if(sender_port != server_port)
        {
            emit terminalWrite(time_str + "[PORT_ERR]: " + QString::number(sender_port) + "\n");
            continue;
        }

        QString data_str_rx = charDatatToHex((uchar *)udp_datagram_rx.data(), udp_size_rx);
        quint8 synch[2];
        synch[0] = udp_datagram_rx.data()[0];
        synch[1] = udp_datagram_rx.data()[1];
        if((synch[0] != 0xff) || (synch[1] != 0x81)) // Ошибка синхронизации
        {
            emit terminalWrite(time_str + "[SYNCH_ERR]: " + data_str_rx + "\n");
            continue;
        }
        if(calculateCRC(udp_datagram_rx.data(), udp_size_rx)) // Ошибка КС
        {
            emit terminalWrite(time_str + "[CRC_ERR]: " + data_str_rx + "\n");
            continue;
        }
        if(udp_size_rx < DATA_SIZE_MIN) // Слишком мало данных
        {
            emit terminalWrite(time_str + "[MSG_ERR]: " + data_str_rx + "\n");
            continue;
        }

        // Обработка кадра
        quint8 msg_cmd_rx = udp_datagram_rx.data()[2];
        quint8 bus_errors = udp_datagram_rx.data()[udp_size_rx-3];
        quint8 mil_std_flags = udp_datagram_rx.data()[udp_size_rx-4];

        QString data_terminal = checkBusErrors(time_str, msg_cmd_rx, bus_errors);

        emit terminalWrite(data_terminal);

        if(msg_cmd_rx == 0xef)
        {
            emit terminalWrite(time_str + "[BUS_ERR]: Ошибка декодирования формата сообщения\n");
        }
        else if(msg_cmd_rx == 0xf1) // Принят кадр подтверждения запуска монитора
        {
            emit setLabelMonitorStatus(MonitorStatus::monitor_ok);
        }
        else if((msg_cmd_rx > 0) && (msg_cmd_rx < 11)) // Принят кадр с форматами сообщений
        {
            quint8 cmd_word1_msb = udp_datagram_rx.data()[3];
            quint8 cmd_word1_lsb = udp_datagram_rx.data()[4];

            quint8 addr_rt = (cmd_word1_msb & 0xf8) >> 3; // Адрес ОУ
            quint8 mode_rx_tx = (cmd_word1_msb & 0x04) >> 2; // 1 - режим ОУ-->КШ, 0 - КШ-->ОУ
            quint8 cmd_code = cmd_word1_lsb & 0x1f; // Число СД / Код команды из таблицы 1 ГОСТ Р52070-2003

            // Для сообщений формата 1, 2, 3, 7, 8 коду cmd_code = 0 соответствует 32
            if(   ((msg_cmd_rx == 1) || (msg_cmd_rx == 2) || (msg_cmd_rx == 3) || (msg_cmd_rx == 7) || (msg_cmd_rx == 8))
               && (cmd_code == 0x00))
            {
                cmd_code = 32;
            }

            QString op_mode("");
            (mode_rx_tx == 1) ? op_mode = "ОУ-->КШ" : op_mode = "КШ-->ОУ";
            if((msg_cmd_rx == 3) || (msg_cmd_rx == 8)) // Для сообщений форматов 3, 8 режим ОУ-->ОУ
                op_mode = "ОУ-->ОУ";

            data_terminal = "";

            // Получаем данные на шине
            QString mil_std_data = data_str_rx.mid(MIL_STD_START);
            int data_size = mil_std_data.size();

            mil_std_data = mil_std_data.left(data_size - SERVICE_SIZE);

            if(!(mil_std_flags & MilStdFlags::RX_OK))
                data_terminal += time_str + mil_std_msg[RX_OK_POS];

            if(!(mil_std_flags & MilStdFlags::CMD1_OK))
                data_terminal += time_str + mil_std_msg[CMD1_OK_POS];

            QString msg_status = "OK";
            if(!checkFormatMsg(msg_cmd_rx, mil_std_flags))
                msg_status = "ERR";

            data_terminal += processFormatMsg(time_str, msg_cmd_rx, mil_std_flags);

            emit terminalWrite(data_terminal);

            // Заполняем таблицу
            QStringList items_list = {time_str, // Время
                                      QString::number(msg_cmd_rx), // Формат сообщения
                                      QString::number(addr_rt), // Адрес ОУ
                                      op_mode, // Режим работы
                                      QString::number(cmd_code), // Число СД / Код команды из таблицы 1 ГОСТ Р52070-2003
                                      mil_std_data, // Запись HEX-данных
                                      msg_status // Статус сообщения
                                     };

            emit setTableMonitorItem(items_list);
            emit setMsgStat(msg_stat_s);
        }

        emit terminalWrite(time_str + "[DATA]: " + data_str_rx + "\n");
    }

} // readPendingDatagrams()

void Client::clearMsgStat()
{
    msg_stat_s = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
}

/******************* END PRIVATE SLOTS FUNCTIONS DEFINITIONS ******************/
