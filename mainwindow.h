#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QTreeWidgetItem>
#include <QDate>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include "client.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void terminalWrite(const QString &data);
    void setLabelMonitorStatus(const Client::MonitorStatus &monitor_status);
    void setTableMonitorItem(const QStringList &data);
    void setMsgStat(const Client::MsgStat &msg_stat);

    void on_pushButton_clearTerm_clicked();
    void on_pushButton_runStopMonitor_clicked();
    void on_pushButton_clearData_clicked();
    void on_pushButton_resetStat_clicked();
    void on_pushButton_saveData_clicked();
    void on_pushButton_saveTerm_clicked();

signals:
    void clearMsgStat(void);

private:
    Ui::MainWindow *ui;
    std::unique_ptr<Client> UdpClient;
    QList<QTreeWidgetItem *> items_stat_list;
    QTime time;
    QDate date;
    QDir dir;

    quint32 row_count = 0;

    const QString mcu_ip = "127.0.0.1";
    const quint16 mcu_port = 4000;

    const quint8 TOTAL_MSG_IND = 0;    
    const quint8 INCORRECT_MSG_IND = 1;
    const quint8 CORRECT_MSG_IND = 2;

    const quint8 FORMAT_MSG_IND = 3;

    const quint8 BC_TO_RT_IND = 4;
    const quint8 FORMAT_01_IND = 5;
    const quint8 FORMAT_07_IND = 6;

    const quint8 RT_TO_BC_IND = 7;
    const quint8 FORMAT_02_IND = 8;

    const quint8 RT_TO_RT_IND = 9;
    const quint8 FORMAT_03_IND = 10;
    const quint8 FORMAT_08_IND = 11;

    const quint8 CMD_CNTRL_IND = 12;
    const quint8 FORMAT_04_IND = 13;
    const quint8 FORMAT_05_IND = 14;
    const quint8 FORMAT_06_IND = 15;
    const quint8 FORMAT_09_IND = 16;
    const quint8 FORMAT_10_IND = 17;

    const quint8 ITEM_TIME_IND = 0;
    const quint8 ITEM_FORMAT_IND = 1;
    const quint8 ITEM_ADDR_RT_IND = 2;
    const quint8 ITEM_OP_MODE_IND = 3;
    const quint8 ITEM_CMD_CODE_IND = 4;
    const quint8 ITEM_DATA_IND = 5;
    const quint8 ITEM_MSG_STATUS_IND = 6;

    const QString total_msg = "Всего сообщений: ";
    const QString format_msg = "Форматы сообщений: ";
    const QString incorrect_msg = "Недостоверные сообщения: ";
    const QString correct_msg = "Достоверные сообщения: ";
    const QString bc_to_rt_msg = "КШ-->ОУ: ";
    const QString rt_to_bc_msg = "ОУ-->КШ: ";
    const QString rt_to_rt_msg = "ОУ-->ОУ: ";
    const QString cmd_cntrl_msg = "КУ: ";
    const QString format_01_msg = "Формат 1: ";
    const QString format_02_msg = "Формат 2: ";
    const QString format_03_msg = "Формат 3: ";
    const QString format_04_msg = "Формат 4: ";
    const QString format_05_msg = "Формат 5: ";
    const QString format_06_msg = "Формат 6: ";
    const QString format_07_msg = "Формат 7: ";
    const QString format_08_msg = "Формат 8: ";
    const QString format_09_msg = "Формат 9: ";
    const QString format_10_msg = "Формат 10: ";

    const QString monitor_fail = "Монитор не запущен";
    const QString monitor_ok = "Монитор запущен";
    const QString label_monitor_fail = "background-color: rgb(225, 225, 225); color: black; border: 1px solid black";
    const QString label_monitor_ok = "background-color: rgb(0, 225, 0); color: black; border: 1px solid black";

    const char *title_data_file = "\tВремя\t\t\tФормат\tАдрес ОУ\tРежим работы\tЧисло СД/Код команды\tДанные(HEX)\tСтатус\n";

    void initPushBtns(void);
    void initTables(void);
    void initLables(void);
    void saveTreeWidgetItems(void);
    void clearTreeWidgetStat(void);
};

#endif // MAINWINDOW_H
