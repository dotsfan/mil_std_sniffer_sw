#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QUdpSocket>
#include <QDate>
#include <memory>
#include <iostream>

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QObject *parent = nullptr);

    void setSocket(const QString ip, const quint16 port);
    QString charDatatToHex(const uchar *char_data, const size_t data_size);
    quint8 calculateCRC(const char *data, const quint64 data_size);
    quint8 sendUdpData(const uchar *udp_data, const quint8 data_size);

    const uchar *getMonitorRunData(void);
    const uchar *getMonitorStopData(void);

    quint8 getMonitorRunDataSize(void);
    quint8 getMonitorStopDataSize(void);

    enum MonitorStatus
    {
        monitor_ok,
        monitor_fail
    };

    struct MsgStat
    {
        quint32 correct;
        quint32 incorrect;

        quint32 bc2rt;
        quint32 rt2bc;
        quint32 rt2rt;
        quint32 cmdCntrl;

        quint32 format01;
        quint32 format02;
        quint32 format03;
        quint32 format04;
        quint32 format05;
        quint32 format06;
        quint32 format07;
        quint32 format08;
        quint32 format09;
        quint32 format10;
    };

signals:
    void terminalWrite(const QString &data);
    void setLabelMonitorStatus(const Client::MonitorStatus &monitor_status);
    void setTableMonitorItem(const QStringList &data);
    void setMsgStat(const Client::MsgStat &msg_stat);

public slots:

private slots:
    void readPendingDatagrams(void);
    void clearMsgStat(void);

private:
    std::unique_ptr<QUdpSocket> client;
    QHostAddress server_ip;
    quint16 server_port = 0;

    struct MsgStat msg_stat_s;
    QTime time;

    const quint8 DATA_SIZE_MIN = 7;
    const quint8 MIL_STD_START = 9;
    const quint8 SERVICE_SIZE = 12;

    const quint8 NORCV_POS = 0;
    const quint8 MANER_POS = 1;
    const quint8 SYNCH_ERR_POS = 2;
    const quint8 SEQ_ERR_POS = 3;
    const quint8 GAP_ERR_POS = 4;
    const quint8 CON_ERR_POS = 5;
    const quint8 PRO_ERR_POS = 6;

    enum BusErrorsReg
    {
        NORCV       = 0x01, /* Ошибка приема */
        MAN_ERR     = 0x02, /* Ошибка декодирования NRZ кода */
        SYNCH_ERR   = 0x04, /* Ошибка синхронизации */
        SEQ_ERR     = 0x08, /* Ошибка в режиме Монитора */
        GAP_ERR     = 0x10, /* Недопустимая активность на шине */
        CON_ERR     = 0x20, /* Ошибка непрерывности сообщения */
        PRO_ERR     = 0x40  /* Ошибка в протоколе в режиме КШ */
    };

    const char *bus_errors_msg[7] = {"[INFO]: Не получено ОС в интервале 14 мкс или не получены ожидаемые данные\n",
                                     "[INFO]: Ошибка в кол-ве принятых бит или ошибка в бите контроля четности\n",
                                     "[INFO]: Ожидался СИ команды, а получен СИ данных или наоборот\n",
                                     "[INFO]: Обнаружено отсутствие ожидаемых данных или пауза при приеме 1-ого слова\n",
                                     "[INFO]: Обнаружена активность на шине в интервале 4 мкс после успешной транзакции\n",
                                     "[INFO]: Передача сообщения не непрерывная\n",
                                     "[INFO]: Недопустимое слово обнаружено на шине во время обмена сообщениями\n"
                                    };

    const quint8 RX_OK_POS = 0;
    const quint8 CMD1_OK_POS = 1;
    const quint8 CMD2_OK_POS = 2;
    const quint8 REPLY1_OK_POS = 3;
    const quint8 REPLY2_OK_POS = 4;
    const quint8 CNTRL_DATA_OK_POS = 5;
    const quint8 DATA_OK_POS = 6;

    enum MilStdFlags
    {
        RX_OK          = 0x01,
        CMD1_OK        = 0x02,
        CMD2_OK        = 0x04,
        REPLY1_OK      = 0x08,
        REPLY2_OK      = 0x10,
        CNTRL_DATA_OK  = 0x20,
        DATA_OK        = 0x40
    };

    const char *mil_std_msg[7] = {"[INFO]: Не принято достоверное слово\n",
                                  "[INFO]: Не принято КС1\n",
                                  "[INFO]: Не принято КС2\n",
                                  "[INFO]: Не принято ОС1\n",
                                  "[INFO]: Не принято ОС2\n",
                                  "[INFO]: Не приняты СД КУ\n",
                                  "[INFO]: Не приняты СД\n"
                                 };

    const quint8 CMD2_ERR_POS = 0;
    const quint8 REPLY1_ERR_POS = 1;
    const quint8 REPLY2_ERR_POS = 2;
    const quint8 CNTRL_DATA_ERR_POS = 3;
    const quint8 DATA_ERR_POS = 4;

    const char *mil_std_err[5] = {"[ERR]: Принято КС2\n",
                                  "[ERR]: Принято ОС1\n",
                                  "[ERR]: Принято ОС2\n",
                                  "[ERR]: Принято СД КУ\n",
                                  "[ERR]: Принято СД\n"
                                 };

    static const quint8 DATA_SYNCH_SIZE = 3;
    static const quint8 DATA_RUN_MONITOR_SIZE = 6;
    static const quint8 DATA_STOP_MONITOR_SIZE = 6;

    const uchar synch[DATA_SYNCH_SIZE] = {0x9e, 0x3c, 0x79};
    const uchar data_monitor_run[DATA_RUN_MONITOR_SIZE] = {synch[0], synch[1], synch[2], 0x02, 0x11, 0x13};
    const uchar data_monitor_stop[DATA_STOP_MONITOR_SIZE] = {synch[0], synch[1], synch[2], 0x02, 0x10, 0x12};

    bool checkFormatMsg(const quint8 format, const quint8 flags);
    QString checkBusErrors(const QString time, const quint8 format, const quint8 bus_err);
    QString processFormatMsg(const QString time, const quint8 format, const quint8 flags);
};

#endif // CLIENT_H
